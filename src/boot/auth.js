import Firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
var authLis

export default ({ router }) => {
  router.beforeEach((to, from, next) => {
    authLis = Firebase.auth().onAuthStateChanged(async () => {
      // console.log('check user', user)
      const { authRequired } = to.meta
      // console.log(authRequired)
      let isAuthenticated = Firebase.auth().currentUser
      // console.log(isAuthenticated.emailVerified)
      if (authRequired) {
        if (isAuthenticated) {
          if (isAuthenticated.emailVerified && authRequired == 'admin') {
            next()
          } else if (authRequired == 'user') {
            next()
          } else {
            next({
              path: '/login'
            })
          }
        } else {
          next({
            path: '/login'
          })
        }
      } else {
        next()
      }
    })
  })

  router.afterEach(() => {
    authLis()
  })
}
