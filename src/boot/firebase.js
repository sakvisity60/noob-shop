import Firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'
import 'firebase/auth'

var firebaseConfig = {
  apiKey: 'AIzaSyCiB2sj8XGQnO2auHhL-5wV6zUqNjf9BI4',
  authDomain: 'noob-shop.firebaseapp.com',
  databaseURL: 'https://noob-shop.firebaseio.com',
  projectId: 'noob-shop',
  storageBucket: 'noob-shop.appspot.com',
  messagingSenderId: '905214755628',
  appId: '1:905214755628:web:602de7fea56d8ca0a06289',
  measurementId: 'G-LFQL44V1CB'
}

export default ({ Vue }) => {
  Firebase.initializeApp(firebaseConfig)
  Vue.prototype.$firebase = Firebase
}
