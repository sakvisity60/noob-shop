const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/Index.vue')
      },
      { path: 'login', component: () => import('pages/Login.vue') },
      { path: 'register', component: () => import('pages/Register.vue') },
      { path: 'cart', component: () => import('pages/Cart.vue') },
      { path: 'order', component: () => import('pages/OrderInfo.vue') },
      { path: 'pay', component: () => import('pages/Pay.vue') }
    ]
  },
  {
    path: '/admin',
    component: () => import('layouts/MyLayout.vue'),
    meta: { authRequired: 'admin' },
    children: [
      { path: '', component: () => import('pages/admin/Main.vue') },
      { path: 'product', component: () => import('pages/admin/Product.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
