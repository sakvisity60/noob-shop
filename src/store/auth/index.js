import Firebase from 'firebase'

export default {
  namespaced: true,

  state: {
    user: {}
  },

  getters: {
    user(state) {
      return state.user
    },

    isAuth(state) {
      return !(Object.keys(state.user).length === 0)
    },
    isAdmin(state) {
      if (!('emailVerified' in state.user)) {
        return false
      }
      return state.user.emailVerified || false
    }
  },

  mutations: {
    SET_USER(state, payload) {
      let user = payload

      state.user = user
    },
    RESET_USER(state) {
      state.user = {}
    }
  },

  actions: {
    signIn({ commit }, payload) {
      let { email, password } = payload

      Firebase.auth()
        .signInWithEmailAndPassword(email, password)
        .then(user => {
          Firebase.firestore()
            .collection('Customers')
            .doc(user.user.uid)
            .get()
            .then(userInfo => {
              const userData = { ...userInfo.data(), ...user.user.toJSON() }
              commit('SET_USER', userData)
              this.$router.push('/')
            })
        })
        .catch(() => {})
    },
    async signOut({ commit }) {
      try {
        Firebase.auth().signOut()
        commit('RESET_USER')
      } catch (error) {
        throw error
      }
    }
  }
}
