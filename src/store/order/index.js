import Firebase from 'firebase'

export default {
  namespaced: true,

  state: {
    order: [],
    payOrder: []
  },

  getters: {
    order(state) {
      return state.order
    },
    payOrder(state) {
      return state.payOrder
    }
  },

  mutations: {
    SET_ORDER(state, docData) {
      state.order = docData
    },
    SET_PAY_ORDER(state, docData) {
      state.payOrder = docData
    }
  },

  actions: {
    fetchOrder({ rootState, commit }) {
      const uid = rootState.auth.user.uid
      Firebase.firestore()
        .collection('Order')
        .where('custommerId', '==', uid)
        .get()
        .then(docs => {
          let docData = []
          docs.forEach(doc => {
            docData.push({
              id: doc.id,
              ...doc.data()
            })
          })
          console.log(docData)
          commit('SET_ORDER', docData)
        })
    },
    fetchPayOrder({ rootState, commit }) {
      const uid = rootState.auth.user.uid
      Firebase.firestore()
        .collection('Order')
        .where('custommerId', '==', uid)
        .where('status', '==', 1)
        .get()
        .then(docs => {
          let docData = []
          docs.forEach(doc => {
            docData.push({
              id: doc.id,
              ...doc.data()
            })
          })
          console.log(docData)
          commit('SET_PAY_ORDER', docData)
        })
    }
  }
}
