import Firebase from 'firebase'

const sendPrice = {
  '1': 30,
  '2': 50,
  '3': 70
}

export default {
  namespaced: true,

  state: {
    products: [],
    cart: []
  },

  getters: {
    products(state) {
      return state.products
    },
    cartSize(state) {
      return state.cart.reduce((total, product) => {
        return total + product.quantity
      }, 0)
    },
    cartTotalAmount(state) {
      return state.cart.reduce((total, product) => {
        return total + product.price * product.quantity
      }, 0)
    },
    cart(state) {
      return state.cart
    }
  },

  mutations: {
    SET_PRODUCT_LIST(state, payload) {
      state.products = payload
    },
    ADD_TO_CART(state, payload) {
      const { uid, name, price } = payload
      // let product = state.products.find(product => product.uid === uid)

      let cartProduct = state.cart.find(product => product.uid === uid)

      if (cartProduct) {
        cartProduct.quantity++
      } else {
        state.cart.push({
          uid,
          name,
          price,
          quantity: 1
        })
      }
    },
    REMOVE_FROM_CART(state, uid) {
      let cartProduct = state.cart.find(product => product.uid === uid)
      if (cartProduct) {
        if (cartProduct.quantity > 0) {
          if (cartProduct.quantity === 1) {
            this.commit('product/DELETE_FROM_CART', uid)
          } else {
            cartProduct.quantity--
          }
        }
      }
    },
    DELETE_FROM_CART(state, uid) {
      let cartProductIndex = state.cart.findIndex(
        product => product.uid === uid
      )
      state.cart.splice(cartProductIndex, 1)
    },
    CLEAR_CART(state) {
      state.cart = []
    }
  },

  actions: {
    fetchProduct({ commit }) {
      let products = []
      Firebase.firestore()
        .collection('Product')
        .get()
        .then(docs => {
          docs.forEach(doc => {
            const { name, price, image, imgRef } = doc.data()
            products.push({
              id: doc.id,
              name,
              price,
              image,
              imgRef
            })
          })
          commit('SET_PRODUCT_LIST', products)
        })
    },
    delProduct({ dispatch }, { uid, imgRef }) {
      Firebase.firestore()
        .collection('Product')
        .doc(uid)
        .delete()
      Firebase.storage()
        .ref()
        .child(imgRef)
        .delete()
        .then(() => {
          dispatch('fetchProduct')
        })
        .catch(() => {
          dispatch('fetchProduct')
        })
    },
    addToCart({ commit }, payload) {
      commit('ADD_TO_CART', payload)
    },
    removeFromCart({ commit }, uid) {
      commit('REMOVE_FROM_CART', uid)
    },
    deleteFromCart({ commit }, uid) {
      commit('DELETE_FROM_CART', uid)
    },
    order({ commit, state, rootState, getters }, sendM) {
      const { uid, name, address, tel } = rootState.auth.user
      const { cart } = state
      const { cartTotalAmount } = getters
      const totalP = cartTotalAmount + sendPrice[sendM]
      Firebase.firestore()
        .collection('Order')
        .add({
          custommerId: uid,
          products: cart,
          orderDate: new Date(),
          confirmDate: null,
          trackId: null,
          slipImg: null,
          totalPrice: totalP,
          status: 1,
          name: name,
          address: address,
          tel: tel,
          sendMethod: sendM
        })
        .then(() => {
          commit('CLEAR_CART')
          this.$router.push('/')
        })
    }
  }
}
