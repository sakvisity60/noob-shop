import Firebase from 'firebase'

export default {
  namespaced: true,

  state: {
    order: []
  },

  getters: {
    order(state) {
      return state.order
    },
    waitOrder(state) {
      return state.order.filter(od => od.status === 1)
    },
    payOrder(state) {
      return state.order.filter(od => od.status === 2)
    },
    sendOrder(state) {
      return state.order.filter(od => od.status === 3)
    },
    receiveOrder(state) {
      return state.order.filter(od => od.status === 4)
    }
  },

  mutations: {
    SET_ORDER(state, order) {
      state.order = order
    }
  },

  actions: {
    fetchOrder({ commit }) {
      Firebase.firestore()
        .collection('Order')
        .get()
        .then(docs => {
          let docData = []
          docs.forEach(doc => {
            docData.push({
              id: doc.id,
              ...doc.data()
            })
          })
          console.log(docData)
          commit('SET_ORDER', docData)
        })
    }
  }
}
